# Gitlab template for generating pdf from markdown files

Based on the following docker containers :
  - pandoc/latex
  - arachnysdocker/athenapdf

## How to use

```yaml
stages:
  - htmlize
  - pdfize

variables:
  PANDOC_CSS_STYLE: style.css
  MARKDOWN_FILES: "*.md"

include:
  - remote: 'https://gitlab.com/documentationtextoriented/markdown2pdf/raw/master/.gitlab-ci.yml'
```

## Variables

| variables        | description                   | required |
|------------------|-------------------------------|----------|
| PANDOC_CSS_STYLE | path to pandoc css style file | yes      |
| MARKDOWN_FILES   | markdown source files         | yes      |


